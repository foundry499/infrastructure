# Foundry499 Infrastructure

**WARNING: This documentation is not complete.**
Jon will be updating it as he improves and finishes the playbooks.

---

This repository contains Ansible playbooks that configure the infrastructure for Foundry499.

- `foundry499.yml` runs `worker.yml` followed by `host.yml`.
- `worker.yml` prepares an OS image for the Foundry499 Workers.
- `host.yml` creates a VM on Azure then deploys the Foundry499 API, Service Queue, and Web App.

# Usage

*Please read this entire document before attempting a deployment.*

## Set Up

1. Ensure Ansible is installed on your local machine. This playbook requires **Ansible 2.8 or later** _(consider [installing Ansible via pip](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-with-pip) instead of your package manager if the packaged version is too old)._
1. Configure admin users according to the [Ops Users](#ops-users) section below.
1. Configure credentials for private container registries according to the [Private Registries](#private-registries) section below.

## Azure Authentication

1. You need an existing Azure Subscription, so [go sign up](https://azure.microsoft.com/en-us/free/) if you don't already have one.
1. Open the [Azure Cloud Shell](https://shell.azure.com) and log in.
1. Once at the shell, run `az ad sp create-for-rbac` which will create an Azure Service Principal.
1. You should see output like this:
    ```
    Creating a role assignment under the scope of "/subscriptions/a9cea03e-1234-5678-9abc-7b5dcb690d7e"
    {
      "appId": "e4db29c1-1234-5678-9abc-7e0447054a51",
      "displayName": "azure-cli-2020-07-16-03-14-41",
      "name": "http://azure-cli-2020-07-16-03-14-41",
      "password": "U77xxvA2Fg_123456789abcdef4l8WqN55",
      "tenant": "6e663c06-1234-5678-9abc-e0e703aab8a7"
    }
    ```
1. Open up `config.yml`, uncomment these four lines, and enter the values from the cloud shell:
    ```yaml
    az_subscription_id: a9cea03e-1234-5678-9abc-7b5dcb690d7e
    az_app_id: e4db29c1-1234-5678-9abc-7e0447054a51
    az_password: U77xxvA2Fg_123456789abcdef4l8WqN55
    az_tenant: 6e663c06-1234-5678-9abc-e0e703aab8a7
    ```
These credentials will be used when you run the playbook and are copied to the host to deploy worker VMs.

## Initial Deployment

Ready for some automation magic?

1. Run the `foundry499` playbook:
    ```shell
    $ ansible-playbook foundry499.yml
    ```
1. Done! :tada:

This playbook does the following:

1. Connects to Azure and creates a new resource group to contain the Foundry499 resources.
1. Provisions resources required for Worker VMs to run later.
1. Provisions a new VM, configures it to be a Worker VM, then images it and destroys it. This allows us to create Worker VMs later from this image that are immediately ready to go.
1. Provisions resources required to run the Foundry499 application stack, including a host VM.
1. Configures the host VM and deploys the Foundry499 application stack.

It will pause once to prompt you for manual input. Configuring DNS is the only thing you need to do manually. Once the public IP address for the new server has been created you'll be shown the details of the DNS record you need to create. Do so, then press `Enter` to continue the playbook.

If you skip it everything will still deploy, but you'll need to SSH in and manually reload Caddy to provision a TLS certificate - until you do this you'll just encounter "invalid certificate" errors when visiting the API or web app. You can reload Caddy by logging in via an ops user then running `sudo systemctl reload caddy`.

## Update Deployments

If you make changes to the deployed services (i.e. the API, service queue, web app, or database) there are two ways to apply these updates to the server.

### Using Ansible

Just run the playbook again. It is idempotent, so anything that is already configured correctly will not be changed. The worker template image **will** be rebuilt, which shouldn't hurt anything but might take a while. You can run the `host.yml` playbook instead fo avoid this.

Docker images for all deployed services are pulled each time the playbook runs, so your new Docker images will be detected/pulled and the services will be restarted automatically. Services without image updates will not be affected.

In summary:
```shell
$ ansible-playbook host.yml
```

### Manually (or via CI)

Update scripts for each container are generated and added to the server automatically. You can use them to update individual services.
The update scripts are called `container-update-NAME`, where `NAME` is the same as the service's `name` in `foundry499.yml`. You can also view logs for each container with scripts called `container-logs-NAME`.

For example, this is the process to update the API:

```bash
# Log in to the server
$ ssh my-server.foundry499.com

# Update the API container
$ container-update-api
Requesting sudo access...
[sudo] password for jonpavelich:
Pulling container image...
Restarting container...
Done!

# View logs for the service queue container
$ container-logs-service
# (log output is displayed, press Q to quit)
```

This script can also be used by the "`gitlab`" user without a sudo password, which allows automatic updates from GitLab CI on the repos of the deployed services.

## Ops Users

Ops users are extra admin users that are included automatically on all servers, including the application host and workers. We've created on for each member of our team, but they're configurable in the playbook's `vars` section. For each ops user you want, you'll need a username, a password hash, and at least one SSH public key.

In order to keep credentials out of version control, lookups to the credential folder are recommended and described here. It is also possible to place the user data directly in the playbook variables instead of using lookups - Ansible Vault is recommended if you take this approach.

1. Create the folder structure `./credentials/default/{users,authorized_keys}` if it does not exist
1. For each user, create a password hash file:
    1. On Linux, run `mkpasswd --method=sha-512` and provide a password
    1. Place the resulting hash in a new file: `credentials/default/users/USERNAME`
1. For each user, create an authorized key file:
    1. Use your existing keypairs, or generate a new SSH key using `ssh-keygen -t ed25519`
    1. Place each public key in a new file: `credentials/default/authorized_keys/USERNAME`
1. Add the user to the playbook variables

```yaml
# Example: two ops users, both using lookups as described in the steps above
vars:
  bootstrap_ops_users:
    - name: jonpavelich
      password_hash: "{{ lookup('file', 'credentials/default/users/jonpavelich') }}"
      authorized_keys: "{{ lookup('file', 'credentials/default/authorized_keys/jonpavelich') }}"

    - name: tharnadek
      password_hash: "{{ lookup('file', 'credentials/default/users/tharnadek') }}"
      authorized_keys: "{{ lookup('file', 'credentials/default/authorized_keys/tharnadek') }}"
```

It's also possible to generate a random password for each user. The following lookup will generate a random 16-character alphanumeric password, save it in plaintext to your local machine at `./credentials/default/users/USERNAME`, generate a random (but repeatable) salt for the hash, then hash the password and use it on the server.

```yaml
password_hash: "{{ lookup('password', 'credentials/default/users/USERNAME length=16 chars=ascii_letters') | password_hash('sha512', 65534 | random(seed=inventory_hostname) | string) }}"
```

_(if you're curious, the random number seeded with the hostname is a way to choose a "random" salt between 1 and 65534 that will differ between hosts but remain the same every time on the same host - this keeps the user update task idempotent, as the salt changing every time (which is the default when no salt is provided) would require a password change every time; [more details here](https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html#hashing-filters))_

## Private Registries

If any of the container registries you want to use require authentication you must configure it.

Same as above: in order to keep credentials out of version control, lookups to the credential folder are recommended and described here.

1. Create the folder structure `credentials/default/container_registries` if it does not exist
1. For each private registry, write the password to a new file: `credentials/default/users/REGISTRY`
1. Add the registries to the playbook variables:

```yaml
# Example: one private registry
vars:
  container_host_registry_auth:
    - registry: registry.gitlab.com
      username: jonpavelich
      password: "{{ lookup('file', 'credentials/default/container_registries/gitlab') }}"
```

# Roles
The Ansible roles included here are sourced from [jonpavelich/ansible](https://gitlab.com/jonpavelich/ansible), which is my own collection of Ansible roles I've been building for personal projects. They have their own README files, some of which are more complete than others:

- [bootstrap](roles/bootstrap/README.md)
- [container_host](roles/container_host/README.md)
- [gitlab_ci_access](roles/gitlab_ci_access/README.md)
- [update](roles/update/README.md)
- [postgres](roles/postgres/README.md)

Upstream changes may be pulled in as needed, but it is not done automatically. This codebase's roles will very likely diverge somewhat from upstream.

Latest revision brought in: [`872ef60a`](https://gitlab.com/jonpavelich/ansible/-/tree/872ef60a05e9945ce18f1b85ff70a6057549a7ae/roles)