# Summary
This role applies bootstrapping steps common to all servers. 
 
# Tasks
- Install common packages
- Configure automatic updates
- Manage user accounts
  - Create ansible user (random sudo password is generated)
  - Create ops users
  - Delete default users
- Configure SSH
  - Access for ops and ansible users
  - System sshd_config
- Copy configuration files
  - terminfo for urxvt265c
  - vimrc

# Variables
| Key                        | Summary                                                     |
| -------------------------- | ----------------------------------------------------------- |
| bootstrap_ssh_user         | User (see below) to configure for use by ansible            |
| bootstrap_ops_users        | Users (see below) to create with ssh/sudo access            |
| bootstrap_remove_users     | List of users to remove from the system if found            |
| bootstrap_install_packages | List of packages to install on the system                   |


The following describes the format of a user entry.
```yaml
name: username
password_hash: hashed password entry
authorized_keys: contents of ~/.ssh/authorized_keys
```

See [defaults/main.yml](defaults/main.yml) for examples.