# Summary
Create PostgreSQL users and databases. Intended to be used with `container_host` role.

# Configuration
Set the `postgresql_create` variable with a list of `user`/`database` pairs. All other variables are optional.

Users and databases will be created if they do not exist, and each database will be owned by its given user.

Passwords will be generated for each user and stored at `credentials/{{inventory_hostname}}/postgres/{{user}}`.

| Key                 | Summary                                                                                 |
| ------------------- | --------------------------------------------------------------------------------------- |
| postgresql_create   | users and databases to create                                                           |
| postgresql_user     | admin user (default: `postgres`)                                                        |
| postgresql_password | admin password (default: lookup `credentials/{{inventory_hostname}}/postgres/postgres`) |
| postgresql_host     | host to connect to (default: `127.0.0.1`)                                               |

## Example
```yaml
vars:
  postgresql_create:
    - user: nextcloud
      database: nextcloud_db
```
