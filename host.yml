---
# This playbook does the following:
#   - Creates the primary resource group if it does not exist
#   - Creates persistent Foundry499 resources: virtual network, storage account
#   - Creates persistent host resources: network security group, subnet, virtual machine, disk
#   - Deploys the Foundry499 application stack

## Play 1: Provision a Virtual Machine on Azure to act as a host
- hosts: localhost
  connection: local
  vars_files:
    - config.yml
  environment: "{{ az_auth_env }}"
  tasks:
    - name: Install required Python modules locally
      pip:
        name:
          - ansible[azure] # required for azure_rm_* modules
          - dnspython # required for lookup('dig')
        extra_args: --user

    - name: Create directories for SSH keypairs
      file:
        path: "{{ item }}"
        state: directory
      loop:
        - "{{ host_ssh_path_ansible }}"
        - "{{ host_ssh_path_gitlab }}"

    - name: Generate SSH keypairs
      openssh_keypair:
        path: "{{ item }}/id_rsa"
        size: 4096
      register: output_ssh_keys
      loop:
        - "{{ host_ssh_path_ansible }}"
        - "{{ host_ssh_path_gitlab }}"

    - name: Set facts for SSH public keys
      set_fact:
        azure_host_ssh_publickey: "{{ output_ssh_keys.results[0].public_key }}"
        azure_host_ssh_filename: "{{ output_ssh_keys.results[0].filename }}"
        gitlab_ssh_publickey: "{{ output_ssh_keys.results[1].public_key }}"
        gitlab_ssh_filename: "{{ output_ssh_keys.results[1].filename }}"

    - name: Create resource group
      azure_rm_resourcegroup:
        name: "{{ az_resource_group }}"
        location: "{{ az_location }}"

    - name: Create storage account
      azure_rm_storageaccount:
        resource_group: "{{ az_resource_group }}"
        name: "{{ az_storage }}"
        type: Premium_LRS

    - name: Create virtual network
      azure_rm_virtualnetwork:
        resource_group: "{{ az_resource_group }}"
        name: "{{ az_vnet }}"
        address_prefixes: "{{ az_vnet_prefix }}"

    - name: Add subnet to virtual network
      azure_rm_subnet:
        resource_group: "{{ az_resource_group }}"
        name: "{{ az_host_subnet }}"
        address_prefix: "{{ az_host_subnet_prefix }}"
        virtual_network: "{{ az_vnet }}"

    - name: Create public IP address
      azure_rm_publicipaddress:
        resource_group: "{{ az_resource_group }}"
        allocation_method: Static
        name: "{{ az_host_ip }}"
      register: output_ip_address

    - name: Set fact for new public IP
      set_fact:
        azure_host_public_ip: "{{ output_ip_address.state.ip_address }}"

    - name: Create network security group
      azure_rm_securitygroup:
        resource_group: "{{ az_resource_group }}"
        name: "{{ az_host_nsg }}"
        rules:
          - name: SSH
            protocol: Tcp
            destination_port_range: 22
            access: Allow
            priority: 1001
            direction: Inbound
          - name: HTTPS
            protocol: Tcp
            destination_port_range: 443
            access: Allow
            priority: 1002
            direction: Inbound
          - name: HTTP
            protocol: Tcp
            destination_port_range: 80
            access: Allow
            priority: 1003
            direction: Inbound

    - name: Create virtual network interface card
      azure_rm_networkinterface:
        resource_group: "{{ az_resource_group }}"
        name: "{{ az_host_nic }}"
        virtual_network: "{{ az_vnet }}"
        subnet: "{{ az_host_subnet }}"
        public_ip_name: "{{ az_host_ip }}"
        security_group: "{{ az_host_nsg }}"

    - name: Create virtual machine
      azure_rm_virtualmachine:
        resource_group: "{{ az_resource_group }}"
        name: "{{ az_host_vm }}"
        vm_size: Standard_DS1_v2
        os_disk_name: "{{ az_host_disk }}"
        managed_disk_type: Premium_LRS
        admin_username: ansible
        ssh_password_enabled: false
        ssh_public_keys:
          - path: /home/ansible/.ssh/authorized_keys
            key_data: "{{ azure_host_ssh_publickey }}"
        network_interfaces: "{{ az_host_nic }}"
        image:
          offer: 0001-com-ubuntu-server-focal # UbuntuServer
          publisher: Canonical
          sku: 20_04-LTS
          version: latest
      register: output_create_vm

    # This is done after VM creation so we can name the disk (the azure_rm_virtualmachine module does not support attaching existing disks or naming new disks)
    # Skipped when the VM has not changed to avoid forcibly detaching and reattaching the disk
    - name: Create and attach a managed data disk
      azure_rm_manageddisk:
        resource_group: "{{ az_resource_group }}"
        name: "{{ az_host_data_disk }}"
        storage_account_type: Premium_LRS
        disk_size_gb: "{{ az_host_data_disk_size }}"
        managed_by: "{{ az_host_vm }}"
      when: output_create_vm.changed

    - name: "Add the created VM as a host"
      add_host:
        name: "{{ azure_host_public_ip }}"
        ansible_host: "{{ azure_host_public_ip }}"
        group: foundry499_host
      changed_when: no

    - name: "Check DNS and prompt user to configure"
      pause:
        prompt: |

          USER ACTION REQUIRED!

          You must configure DNS now so that a TLS certificate may be provisioned during the next phase.
          If you do not configure DNS before continuing the playbook will still complete successfully, but
          HTTPS connections to the app will fail until provisioning is automatically reattempted by Caddy.

          Please create the following records.

            NAME                   TYPE    VALUE
            {{ foundry_api_url }}.    A       {{ azure_host_public_ip }}
            {{ foundry_app_url }}.    A       {{ azure_host_public_ip }}

          Press Enter to continue once the record is configured, or Ctrl-C to stop the playbook.
      when: (lookup('dig', foundry_api_url) != azure_host_public_ip) or (lookup('dig', foundry_app_url) != azure_host_public_ip)


## Play 2: Configure the virtual machine to act as the Foundry499 Host
- hosts: foundry499_host
  vars_files:
    - config.yml
  vars:
    ansible_user: ansible
    ansible_ssh_private_key_file: "{{ hostvars['localhost']['azure_host_ssh_filename'] }}"
    ansible_ssh_extra_args: -o StrictHostKeyChecking=no # avoid prompting the user to accept a host key (it'll change during this configuration play)

    # This section defines the Ansible user
    bootstrap_ssh_user:
      name: ansible
      password_hash: "{{ lookup('password', 'credentials/host/users/ansible length=16 chars=ascii_letters') | password_hash('sha512', 65534 | random(seed=inventory_hostname) | string) }}"
      authorized_keys: "{{ hostvars['localhost']['azure_host_ssh_publickey'] }}"

    # This keypair will be used by GitLab CI to update the host containers
    gitlab_ci_authorized_keys: "{{ hostvars['localhost']['gitlab_ssh_publickey'] }}"

    # This section defines directories to create that will be mounted on containers
    container_host_create_mounts:
      - /data/postgresql/data

    # This section defines the services to host
    container_host_services:
      - name: postgres
        image: docker.io/library/postgres
        host_port: 5432
        container_port: 5432
        extra_args: -v /data/postgresql/data:/var/lib/postgresql/data
        env: |
          POSTGRES_PASSWORD={{ lookup('password', 'credentials/host/postgres/postgres length=16 chars=ascii_letters') }}

      - name: api
        image: registry.gitlab.com/foundry499/api/api:latest
        host_url: "{{ foundry_api_url }}"
        host_port: 8080
        container_port: 8080
        extra_caddy: |
          header * {
              Access-Control-Allow-Origin https://{{ foundry_app_url }}
              Access-Control-Allow-Credentials true
              Access-Control-Allow-Methods "POST, PUT, GET, OPTIONS, DELETE"
              Access-Control-Allow-Headers "Content-Type"
              Set-Cookie "Path=" "Secure; Path="
              defer
          }
          header /docs/* {
              Access-Control-Allow-Origin *
              Access-Control-Allow-Credentials false
              -Access-Control-Allow-Methods
              -Access-Control-Allow-Headers
              defer
          }
        env: |
          SESSION_KEY={{ lookup('password', 'credentials/host/api/session_key length=32 chars=ascii_letters') }}
          DB_HOST=container-host
          DB_PORT=5432
          DB_USER=foundry
          DB_PASSWORD={{ lookup('password', 'credentials/host/postgres/foundry length=16 chars=ascii_letters') }}
          DB_NAME=foundry

      - name: service
        image: registry.gitlab.com/foundry499/api/service:latest
        env: |
          DB_HOST=container-host
          DB_PORT=5432
          DB_USER=foundry
          DB_PASSWORD={{ lookup('password', 'credentials/host/postgres/foundry length=16 chars=ascii_letters') }}
          DB_NAME=foundry
          AZURE_TENANT_ID={{ az_tenant }}
          AZURE_CLIENT_ID={{ az_app_id }}
          AZURE_CLIENT_SECRET={{ az_password }}
          AZURE_TIMEOUT=240
          AZURE_SUBSCRIPTION_ID={{ az_subscription_id }}
          AZURE_RESOURCE_GROUP={{ az_resource_group }}
          AZURE_LOCATION={{ az_location }}
          AZURE_WORKER_NETWORK={{ az_vnet }}
          AZURE_WORKER_SUBNET={{ az_worker_subnet }}
          AZURE_WORKER_SECURITY_GROUP={{ az_worker_nsg }}
          AZURE_WORKER_IMAGE=/subscriptions/{{ az_subscription_id }}/resourceGroups/{{ az_resource_group }}/providers/Microsoft.Compute/images/{{ foundry_worker_image }}
          SSH_USER=api
          SSH_PRIVATE_KEY={{ lookup('file', '{{ worker_ssh_path_api}}/id_rsa') | regex_replace('\n', '\\n') }}
          SSH_SOCKET=unix://{{ az_worker_socket }}

      - name: webapp
        image: registry.gitlab.com/foundry499/web-app:latest
        host_url: "{{ foundry_app_url }}"
        host_port: 8081
        container_port: 8000
        env: |
          REACT_APP_API_LOCATION=https://{{ foundry_api_url }}

    # This section avoids (re)starting the API containers until the database exists
    container_host_defer_restart:
      - api
      - service

    # This section creates the Foundry499 database
    postgresql_create:
      - user: foundry
        database: foundry

  tasks:
    - include_role:
        name: bootstrap

    - name: Install parted
      become: yes
      package:
        name: parted
        state: present

    - name: Check the data drive state
      become: yes
      parted: device=/dev/disk/azure/scsi1/lun0 unit=MiB
      register: output_disk_info
      changed_when: no

    - name: Partition the data drive
      become: yes
      parted:
        device: /dev/disk/azure/scsi1/lun0
        label: gpt
        number: 1
        state: present
        part_end: 100%
      when: (output_disk_info.partitions | length) == 0

    - name: Format the data drive
      become: yes
      filesystem:
        fstype: ext4
        dev: /dev/disk/azure/scsi1/lun0-part1

    - name: Mount the data drive
      become: yes
      mount:
        path: /data
        src: /dev/disk/azure/scsi1/lun0-part1
        fstype: ext4
        state: mounted

    - include_role:
        name: gitlab_ci_access

    - include_role:
        name: container_host

    - include_role:
        name: postgresql

    # Start the deferred containers now that the database is configured
    - name: "containers | restart modified deferred containers"
      become: yes
      become_user: podman
      systemd:
        scope: user
        name: "container-{{ item }}.service"
        state: restarted
      loop: "{{ container_host_deferred_restart_required }}"

## Play 3: Print summary
- hosts: localhost
  connection: local
  tasks:
    - name: Print path to generated SSH keys
      debug:
        msg: "Generated SSH keypair for ansible is at ./{{ azure_host_ssh_filename }} and for gitlab is at ./{{ gitlab_ssh_filename }}"

    - name: Print public IP for created VM
      debug:
        msg: "The public IP for the host is {{ azure_host_public_ip }}"
...